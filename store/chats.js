import moment from 'moment'

export const state = () => ({
	chats: []
})

export const mutations = {
	setChats(state) {
		state.chats = [
			{
				profile: {
					avatar: require('~/assets/img/temp/avatar-bener.jpeg'),
					name: 'Doctor Bener'
				},
				isMaster: false,
				message: 'Hello, my fiend',
				time: '15:00 PM'
			},
			{
				profile: {
					avatar: require('~/assets/img/temp/avatar-tony.jpeg'),
					name: 'Tony Stark'
				},
				isMaster: true,
				message: 'Hello!',
				time: '15:01 PM'
			},
			{
				profile: {
					avatar: require('~/assets/img/temp/avatar-bener.jpeg'),
					name: 'Doctor Bener'
				},
				isMaster: false,
				message: 'How are you?',
				time: '15:01 PM'
			}
		]
	},
	sendMessage(state, payload) {
		state.chats.push(payload)
	}
}

export const actions = {
	async fetchChats({ commit }) {
		await window.setTimeout(() => {
			commit('setChats')
		}, 1500)
	},
	async sendMessage({ commit }, payload) {
		commit('sendMessage', {
			profile: {
				avatar: require('~/assets/img/temp/avatar-tony.jpeg'),
				name: 'Tony Stark'
			},
			isMaster: true,
			message: payload,
			time: moment(new Date()).format('hh:mm A')
		})
		await window.setTimeout(() => {
			commit('sendMessage', {
				profile: {
					avatar: require('~/assets/img/temp/avatar-bener.jpeg'),
					name: 'Doctor Bener'
				},
				isMaster: false,
				message: 'Excellent!',
				time: '15:01 PM'
			})
		}, 1500)
	}
}

export const getters = {
	getChats: state => state.chats
}
