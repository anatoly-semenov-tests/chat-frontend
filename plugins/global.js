import Vue from 'vue'
import UiAvatar from '~/components/ui-kit/ui-avatar.vue'

Vue.component('ui-avatar', UiAvatar)
