module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: "babel-eslint"
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    "google",
    "eslint:recommended",
    "plugin:vue/essential",
    "plugin:prettier/recommended"
  ],
  // required to lint *.vue files
  plugins: ["vue"],
  // add your custom rules here
  rules: {
    indent: ["error", "tab", { SwitchCase: 1 }],
    semi: ["error", "never"],
    "no-mixed-spaces-and-tabs": ["error", "smart-tabs"],
    "no-console": 0,
    "no-tabs": 0,
    "linebreak-style": 0,
    "vue/html-indent": ["error", "tab"]
  }
}
